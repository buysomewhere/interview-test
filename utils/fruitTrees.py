class AbstractTree:


    def grow(self):
        """
        Causes the tree to grow, taking resources from the environment
        @return the amount of water it uses
        @rtype: int
        """
        raise NotImplementedError("Implement this in a subclass")

    def kill(self):
        """
        Kills the tree. The trees status information should report that it is killed and it should use 0 water when
        the grow opperation is called.
        """
        raise NotImplementedError("Implement this in a subclass")

    def status(self):
        """
        Returns a string with the status of the tree
        @return a string with the status of the tree
        """
        return "Tree type %s, height %f, state %s"%(self.type(), self.height(), self.state())

    def height(self):
        """
        Return the height of the tree, it should be increased by the grow function
        @return the height of the tree
        @rtype: float
        
        """
        raise NotImplementedError("Implement in a subclass")

    def type(self):
        """
        Returns the name of the tree type in human readable form as it is defined in the spec
        @return: the name (E.g. "Simple Tree")
        @rtype: str
        """
        raise NotImplementedError("Implement in a subclass")

    def state(self):
        """
        Returns the state of the tree as a string. (Killed or Growing)
        @return str of either "Killed" or "Growing"
        """
        raise NotImplementedError("Implement in a subclass")
    
    def setOrchard(self, aOrchard):
        """
        Tells the tree it has been added to an orchard
        
        @param aOrchard: the orchard the tree is being added to 
        @type aOrchard: L{Orchard}
        
        """
        raise NotImplementedError("Implemnt in a subclass")


class SimpleTree(AbstractTree):
    pass


class OrangeTree(AbstractTree):
    pass


class PearTree(AbstractTree):
    pass


class PlumbTree(AbstractTree):
    pass


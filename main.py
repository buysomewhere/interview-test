#! /usr/bin/python

"""
Your task:
- build an orchard simulator using the provided stubs. You may need to modify the step function.

The Orchard has the following trees with the following properties, you need to demonstrate a few different test sets.
In each step of the simulation each tree will use a certain amount of water, if there is not enough water the tree will 
die water is added in each step and is cumulative.

Trees are interacted with through the AbstractTree class.

You will need to add functions to the Orchard to allow the trees to access its status. You may also need to add functions
to the tree class. You may modify and add classes to fruitTrees.py

The trees are as following:
- Simple Tree - consumes 1L of water every step and grows by 1m every step
- Orange Trees - consume 1 * (total number of living orange trees in the orchard) L of water every step and grow
  (1/number of living simple trees) m every step. If there are no simple trees they grow 1m every step. 
- Pear Trees - consume 1L of water for every 1m of height of all the trees in the forest combined, they grow a step
   in the fibonacci sequence each turn (starting with height 0 on step 0, have height 1 on step 1, 1 on step 2, 2 on step 3,...).
- Plumb Trees (Extra Hard) - consume 0.001L of water for every second since the unix epoch each step, they grow 1cm for every degree
   C above 0C of the maximum temperature in Sydney according to the accuweather api.  (See
   http://developer.accuweather.com/accuweather-forecast-api/apis/get/forecasts/v1/daily/1day/%7BlocationKey%7D
   API key is 1zhfJpwtY3ASzmF95Z01j40NAU8DWIaV Sydney is location 13007). Note this key is limited to 50 requests a day,
   you can create you own if you want more. You can use any python http api (E.g. requests) and will need to parse their json
   

There is one bug in the step function you need to fix   

Please follow the following style rules:

- All class names start with a capital letter
- All function parameters start with an 'a' (with the exception of self). E.g. "def someFunc(aParam1, aParam2):"
- All variable names shall be camel case and start with a lower case letter
- Private variables should begin with an '_'
- All functions should have comments describing their purpose and paramters
- You may not use global variables (note that you may use class variables)

Test Rules:

- You have 2.5 hours do as much as you can.
- You may use third party libraries that are available via pip. If you do please add a note with what libraries you used 
    (E.g. requests, httplib, etc)
- Use python 2.7
- You *are* expected to use Google, etc in this test. 
- Its ok if not all the trees are implemented do what you can in the time
- You must write a few tests for each tree as you implement it. This is part of what you are being assessed on.
- You may modify fruitTrees.py freely, but your implementation of any given tree should work with any implementation of
    the other trees (i.e your OrangeTree should work with someone else's PearTree)
    
To Run Unit Tests:
pip install unittest
python -m unittest -v test_orchard

"""


class Orchard:
    """
    Defines an Orchard.
    Note that trees and water are public variables and you may access them 
    """

    def __init__(self, aTrees, aInitialWater):
        """
        Creates a new Orchard with a set of trees and a certain initial water value
        @param aTrees: An array of trees of different types
        @type aTrees: list of L{AbstractTree}

        @param aInitialWater: the initial amount of water provided
        @type aInitialWater: int
        """
        self.trees = aTrees
        for tree in self.trees:
            tree.setOrchard(self)
        self.water = aInitialWater

    def step(self, aWaterAdded):
        """
        Called on each iteration of the simulations
        @param aWaterAdded: how much water is added to the simulation
        @type aWaterAdded: int

        @return: if all the trees have died return false, otherwise return true
        @rtype: bool
        """
        self.water += aWaterAdded

        deadTrees = 0

        for tree in self.trees:
            if tree.state() != "Killed":
                if self.water <= 0:
                    tree.kill()
                else:
                    waterLoss = tree.grow()
                    if (self.water - waterLoss) < 0:
                        print waterLoss
                        tree.kill()
                    else:
                        self.water -= waterLoss
            print tree.status()

            # since the tree can die during this step its easier to do this check twice
            if tree.state() == "Killed":
                deadTrees += 1

        if deadTrees >= len(self.trees):
            return False
        return True

    def runOrchard(self, aSteps, aWaterAdded):
        """
        Runs the orchard for aSteps, or until all the trees are dead.
        Note that this does not reset the simulation (it can be called many times)
        @param aSteps: the number of steps to run the simulation
        @type aSteps: int

        @param aWaterAdded: how much water is added each step
        @type aWaterAdded: int

        @return: false if all the trees have died otherwise True
        @rtype: bool
        """
        for i in range(0, aSteps):
            notDead = self.step(aWaterAdded)
            if not notDead:
                return False
        return True
    
    def getTotalHeight(self):
        """
        Returns the total height of all living trees in the orchard
        @return: the height of all living trees
        @rtype: float
        """
        height = 0.0
        for tree in self.trees:
            if tree.state() != "Killed":
                height += tree.height()
        return height

if __name__ == 'main':
    orchard = Orchard([], 1000) #add trees here
    for i in range(0, 10):
        orchard.step(5)
        print "----Step %d, complete---"%i



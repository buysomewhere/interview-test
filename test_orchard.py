"""
    @author: (Original Author) Harry J.E Day <harry@buysomewhere.com>
    @since: 23/06/17
    @summary: Basic Tests of the orchard
"""

import unittest

from main import Orchard
from utils.fruitTrees import AbstractTree, SimpleTree, OrangeTree, PearTree, PlumbTree


class TestOrchard(unittest.TestCase):
    """
    Add your own tests here!
    There are a few basic tests for simple tree and one test for orange tree.
    A test function must start with test_
    """

    def test_treeTypes(self):
        """
        Tests the type() method for all trees
        """
        self.assertEqual(SimpleTree().type(), "Simple Tree")
        self.assertEqual(OrangeTree().type(), "Orange Tree")
        self.assertEqual(PearTree().type(), "Pear Tree")
        self.assertEqual(PlumbTree().type(), "Plumb Tree")

    
    def test_simpleTreeSim(self):
        """
        Really basic test. The trees have enough water for 1 turn, on the second turn they die
        """
        orchard = Orchard(
            [SimpleTree() for i in range(0, 10)],
            10
        )
        # these should die on the second step
        self.assertTrue(orchard.runOrchard(1,0))
        self.assertEqual(orchard.getTotalHeight(), 10)
        self.assertFalse(orchard.runOrchard(1,0))

    def test_simpleTreeSimWater(self):
        """
        Really basic test. The trees have enough water for 1 turn and we add enough water for one tree on the second turn.
        The rest die and then the first tree dies on the third step
        """
        orchard = Orchard(
            [SimpleTree() for i in range(0, 10)],
            10
        )

        self.assertTrue(orchard.runOrchard(1,0))
        self.assertEqual(orchard.getTotalHeight(), 10)
        self.assertTrue(orchard.runOrchard(1,1))
        self.assertEqual(orchard.getTotalHeight(), 2)
        self.assertFalse(orchard.runOrchard(1,0))

    def test_longRunningSimpleTest(self):
        """
        Trees initially get enough water to sustain them each step. Then we take that away.
        """
        orchard = Orchard(
            [SimpleTree() for i in range(0, 20)],
            20
        )
        self.assertTrue(orchard.runOrchard(10, 20))
        self.assertEqual(orchard.getTotalHeight(), 10 * 20)
        self.assertTrue(orchard.runOrchard(11, 5))
        self.assertEqual(orchard.getTotalHeight(), 5 * 21)
        self.assertFalse(orchard.runOrchard(1, 0))

    def test_basicOrangeTreeWithSimples(self):
        """
        Really basic test. The trees have enough water for 1 turn, on the second turn they die
        """
        orchard = Orchard(
            [SimpleTree() for i in range(0, 10)] + [OrangeTree()],
            11
            )
        # these should die on the second step
        self.assertTrue(orchard.runOrchard(1,0))
        self.assertEqual(orchard.getTotalHeight(), 10 + (1.0/10))
        self.assertFalse(orchard.runOrchard(1,0))

    def test_twoOrangeTrees(self):
        """
        Tests the growth and water consumption of two orange trees
        """
        orchard = Orchard(
            [OrangeTree() for i in range(0, 2)],
            7
        )
        self.assertTrue(orchard.runOrchard(1,0))
        self.assertTrue(orchard.runOrchard(1,0))
        self.assertTrue(orchard.runOrchard(1,0))
        self.assertFalse(orchard.runOrchard(1,0))

    def test_BasicPlumTree(self):
        """
        Tests the growth and water consumption of two Plumb Trees
        """
        orchard = Orchard(
            [PearTree() for i in range(0, 2)],
            1
        )
        self.assertTrue(orchard.runOrchard(1,0)) # they consume no water on the first step because the height is 0
        self.assertEqual(orchard.getTotalHeight(), 0)
        self.assertTrue(orchard.runOrchard(1, 0)) # the second tree consumes 1L of water because the first tree will have height one when it runs
        self.assertEqual(orchard.getTotalHeight(), 2)
        self.assertTrue(orchard.runOrchard(1, 4)) # the second tree consumes 2L and the first consumes 2L
        self.assertEqual(orchard.getTotalHeight(), 2)
        self.assertFalse(orchard.runOrchard(1, 0))
